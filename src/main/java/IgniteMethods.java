import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterGroup;

import org.apache.ignite.configuration.IgniteConfiguration;

import javax.cache.Cache.Entry;
import java.util.Collection;
import java.util.LinkedList;


public class IgniteMethods {


    IgniteConfiguration cfg = new IgniteConfiguration();
    Ignite ignite = null;
    Ignite server = null;
    IgniteCompute compute = null;
    //CacheConfiguration cCfg = new CacheConfiguration();

    public void ConfigureClient() {
        cfg.setClientMode(true);
        //cCfg.setName("students");
        ignite = Ignition.start(cfg);
        ClusterGroup clientGroup = ignite.cluster().forClients();
    }

    public void ConfigureServer() {
        cfg.setClientMode(false);
        //cCfg.setName("students");
        ignite = Ignition.start(cfg);
        compute = ignite.compute();
    }

    public void CloseConnection() {
        Ignition.stop(ignite.name(), false);
    }

    public void IPutStudent(String imie, String surname, long pesel, int dataUr, int grupa_id) {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        IgniteCache<Long, Grupa> grupy = ignite.getOrCreateCache("grupy");
        Grupa grupa = grupy.get(Long.valueOf(grupa_id));
        if (grupa != null) {
            long key = 0;
            for (Entry<Long, Student> e : students) {
                key = e.getKey();
            }

            Student _student = new Student(imie, surname, pesel, dataUr, grupa_id);
            students.put(++key, _student);
            long finalKey = key;
            if (!cfg.isClientMode())
                compute.broadcast(() -> System.out.println("PUT " + finalKey + " => " + _student));
            else
                System.out.println("PUT " + finalKey + " => " + _student);
        } else if (!cfg.isClientMode())
            compute.broadcast(() -> System.out.println("Nieprawidłowe Id grupy"));
        else
            System.out.println("Nieprawidłowe Id grupy");
    }

    public void IGetStudent() {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        long key = students.size() + 1;
        System.out.println("All students: ");
        for (Entry<Long, Student> e : students) {
            System.out.println(e.getKey() + " => " + e.getValue());
        }
    }

    public void IGetStudent(int i) {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        long key = students.size() + 1;
        System.out.println("Student with key:" + i);
        for (Entry<Long, Student> e : students) {
            if (e.getKey() == i) {
                System.out.println(e.getKey() + " => " + e.getValue());
            }
        }
    }

    public void IEditStudent(long i, String name, String surname, int grupa) {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        Student _s = students.get(i);
        _s.setName(name);
        _s.setSurname(surname);
        _s.setGrupa(grupa);
        students.put(i, _s);
    }

    public void IEvictStudent(long key) {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        System.out.println("Deleted " + key + " => " + students.get(key));
        students.remove(key);
    }

    public void IEvictAllStudent() {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        students.removeAll();
        System.out.println("Wszyscy studenci zostali usunięci");
    }

    public void IPutGroup(String nazwa, int rocznik) {
        IgniteCache<Long, Grupa> grupy = ignite.getOrCreateCache("grupy");
        long key = 0;
        if (grupy != null) {
            for (Entry<Long, Grupa> e : grupy) {
                key = e.getKey();
            }
        }
        Grupa _grupa = new Grupa(nazwa, rocznik);
        grupy.put(++key, _grupa);
        long finalKey = key;
        if (!cfg.isClientMode())
            compute.broadcast(() -> System.out.println("PUT " + finalKey + " => " + _grupa));
        else
            System.out.println("PUT " + finalKey + " => " + _grupa);
    }

    public void IGetGroup() {
        IgniteCache<Long, Grupa> grupy = ignite.getOrCreateCache("grupy");
        long key = grupy.size() + 1;
        System.out.println("Wszystkie grupy: ");
        for (Entry<Long, Grupa> e : grupy) {
            System.out.println(e.getKey() + " => " + e.getValue());
        }
    }

    public void IGetAllStudentsFromGroup(int grupa_id) {
        IgniteCache<Long, Student> students = ignite.getOrCreateCache("students");
        Collection<Student> _students = new LinkedList<Student>();
        for (Entry<Long, Student> s : students) {
            if (s.getValue().getGrupa() == grupa_id)
                _students.add(s.getValue());
        }

        System.out.println("Wszyscy studenci z wybranej grupy");
        for (Student s : _students) {
            System.out.println(s);
        }
    }

}
