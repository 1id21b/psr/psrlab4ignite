import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab4Ignite {

    public static void main(String[] args) throws IOException {
        IgniteMethods hcm = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int lastConfig = 0;

        String s = "";

        while (!s.equals("q")) {
            ShowMenu();
            s = br.readLine();
            if (s.equals(String.valueOf(1))) {
                hcm = new IgniteMethods();
                hcm.ConfigureClient();

                while (!s.equals("b")) {
                    ShowSubMenu();
                    s = br.readLine();

                    switch (s) {
                        case "d":
                            System.out.println("Wpisz Imie:");
                            String imie = br.readLine();
                            System.out.println("Wpisz Nazwisko:");
                            String nazwisko = br.readLine();
                            System.out.println("Wpisz Pesel:");
                            String pesel = br.readLine();
                            System.out.println("Wpisz Date:");
                            String data = br.readLine();
                            System.out.println("Wprowadz id grupy:");
                            String grupa_id = br.readLine();
                            hcm.IPutStudent(imie, nazwisko, Long.valueOf(pesel), Integer.valueOf(data), Integer.valueOf(grupa_id));
                            break;
                        case "u":
                            hcm.IEvictAllStudent();
                            break;
                        case "u1":
                            System.out.println("Podaj Id osoby do usunięcia");
                            String Id = br.readLine();
                            hcm.IEvictStudent(Long.valueOf(Id));
                            break;
                        case "e":
                            System.out.println("Podaj Id osoby do edycji");
                            Id = br.readLine();
                            System.out.println("Podaj nowe Imie osoby");
                            imie =  br.readLine();
                            System.out.println("Podaj nowe Nazwisko osoby");
                            nazwisko = br.readLine();
                            System.out.println("Podaj nowa grupe osoby");
                            grupa_id = br.readLine();
                            hcm.IEditStudent(Long.valueOf(Id),imie, nazwisko, Integer.valueOf(grupa_id));
                            break;
                        case "w":
                            hcm.IGetStudent();
                            break;
                        case "w1":
                            System.out.println("Podaj Id osoby do wyswietlenia");
                            Id = br.readLine();
                            hcm.IGetStudent(Integer.valueOf(Id));
                            break;
                        case "g":
                            System.out.println("Wpisz Nazwe grupy:");
                            String nazwa = br.readLine();
                            System.out.println("Wpisz rocznik grupy:");
                            String rocznik = br.readLine();
                            hcm.IPutGroup(nazwa, Integer.valueOf(rocznik));
                            break;
                        case "wg":
                            hcm.IGetGroup();
                            break;
                        case "sg":
                            System.out.println("Wprowadz Id grupy:");
                            String id = br.readLine();
                            hcm.IGetAllStudentsFromGroup(Integer.valueOf(id));
                            break;
                        case "b":
                            hcm.CloseConnection();
                            break;
                    }
                }
            } else if (s.equals(String.valueOf(2))) {
                hcm = new IgniteMethods();
                hcm.ConfigureServer();


                while (!s.equals("b")) {
                    ShowSubMenu();
                    s = br.readLine();

                    switch (s) {
                        case "d":
                            System.out.println("Wpisz Imie:");
                            String imie = br.readLine();
                            System.out.println("Wpisz Nazwisko:");
                            String nazwisko = br.readLine();
                            System.out.println("Wpisz Pesel:");
                            String pesel = br.readLine();
                            System.out.println("Wpisz Date:");
                            String data = br.readLine();
                            System.out.println("Wprowadz id grupy:");
                            String grupa_id = br.readLine();
                            hcm.IPutStudent(imie, nazwisko, Long.valueOf(pesel), Integer.valueOf(data), Integer.valueOf(grupa_id));
                            break;
                        case "u":
                            hcm.IEvictAllStudent();
                            break;
                        case "u1":
                            System.out.println("Podaj Id osoby do usunięcia");
                            String Id = br.readLine();
                            hcm.IEvictStudent(Long.valueOf(Id));
                            break;
                        case "e":
                            System.out.println("Podaj Id osoby do edycji");
                            Id = br.readLine();
                            System.out.println("Podaj nowe Imie osoby");
                            imie =  br.readLine();
                            System.out.println("Podaj nowe Nazwisko osoby");
                            nazwisko = br.readLine();
                            System.out.println("Podaj nowa grupe osoby");
                            grupa_id = br.readLine();
                            hcm.IEditStudent(Long.valueOf(Id),imie, nazwisko, Integer.valueOf(grupa_id));
                            break;
                        case "w":
                            hcm.IGetStudent();
                            break;
                        case "w1":
                            System.out.println("Podaj Id osoby do wyswietlenia");
                            Id = br.readLine();
                            hcm.IGetStudent(Integer.valueOf(Id));
                            break;
                        case "g":
                            System.out.println("Wpisz Nazwe grupy:");
                            String nazwa = br.readLine();
                            System.out.println("Wpisz rocznik grupy:");
                            String rocznik = br.readLine();
                            hcm.IPutGroup(nazwa, Integer.valueOf(rocznik));
                            break;
                        case "wg":
                            hcm.IGetGroup();
                            break;
                        case "sg":
                            System.out.println("Wprowadz Id grupy:");
                            String id = br.readLine();
                            hcm.IGetAllStudentsFromGroup(Integer.valueOf(id));
                            break;
                        case "b":
                            hcm.CloseConnection();
                            break;
                    }
                }
            }
        }
    }

    private static void ShowMenu() {
        System.out.println("Menu:");
        System.out.println("Dla operacji po stronie klienta wybierz \"1\"");
        System.out.println("Dla operacji po stronie składu wybierz \"2\"");
        System.out.println("Aby zakończyć wybierz \"q\"");
    }

    private static void ShowSubMenu() {
        System.out.println("SubMenu:");
        System.out.println("Dodaj grupę \"g\"");
        System.out.println("Wyswietl wszystkie grupy \"wg\"");
        System.out.println("Wyswietl wsyzstkin studentów w danej grupei \"sg\"");
        System.out.println("Dla operacji dodania wybierz \"d\"");
        System.out.println("Dla operacji usuniecia wszystich studentów wybierz \"u\"");
        System.out.println("Dla operacji usuniecia studenta po Id wybierz \"u1\"");
        System.out.println("Dla operacji edycji wybierz \"e\"");
        System.out.println("Dla operacji wyświetlenia wybierz \"w\"");
        System.out.println("Dla operacji wyświetlenia jednej osoby wybierz \"w1\"");
        System.out.println("Aby wrocic wybierz \"b\"");
    }
}
