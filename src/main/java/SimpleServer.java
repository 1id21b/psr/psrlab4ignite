import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;

import java.io.IOException;

public class SimpleServer {

    public static void main(String[] args) throws IOException {

        IgniteConfiguration cfg = new IgniteConfiguration();
        Ignite ignite = null;

        cfg.setClientMode(false);
        ignite = Ignition.start(cfg);


    }
}
