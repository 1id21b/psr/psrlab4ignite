import java.io.Serializable;

public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String surname;
    private long pesel;
    private int birthyear;
    private int grupa;



    public Student(String _name, String _surname, long _pesel, int _birthyear, int _grupa) {
        this.name = _name;
        this.surname = _surname;
        this.birthyear = _birthyear;
        this.pesel =_pesel;
        this.grupa = _grupa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(int birthyear) {
        this.birthyear = birthyear;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    public void setGrupa(int grupa) {
        this.grupa = grupa;
    }

    public String getSurname() {
        return surname;
    }

    public long getPesel() {
        return pesel;
    }

    public int getGrupa() {
        return grupa;
    }

    @Override
    public String toString(){
        return "Student " + name + " " + surname + " - pesel: " + pesel + " urodzony w roku " + birthyear + " grupa " + grupa;
    }
}
